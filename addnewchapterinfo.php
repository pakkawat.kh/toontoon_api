<?php
require_once('connection.php');
$_POST = json_decode(file_get_contents("php://input"),true);
$id = $_POST['id'];
$chapter = $_POST['chapter'];
$cartoonfile = $_POST['cartoonFile'];
$orderid = $_POST['orderid'];
$db->insert("chapter", [
"bookid"=>$id,
"chapter"=>$chapter,
"cartoonfile"=>$cartoonfile,
"orderid"=>$orderid
]);
$insertId = $db->id();
//update date and time in book
$date = date('d/m/Y H:i:s', time());
$db->update("book",[
    "timestamp"=>$date,
    "lastchapter"=>$chapter
],[
    "bookid"=>$id
]);
echo $insertId;

?>